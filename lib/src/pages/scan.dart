import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:VacunatePeru/src/pages/direcciones_page.dart';
import 'package:VacunatePeru/src/pages/mapas_page.dart';

// import 'package:atualcanceperu/providers/db_provider.dart';
import 'package:VacunatePeru/src/providers/scan_list_provider.dart';
import 'package:VacunatePeru/src/providers/ui_provider.dart';

import 'package:VacunatePeru/src/widgets/custom_navigatorbar.dart';
import 'package:VacunatePeru/src/widgets/scan_button.dart';
import 'package:VacunatePeru/src/widgets/menu_widget.dart';

// import 'package:camera/camera.dart';

class ScanPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('Historial'),
        actions: [
          IconButton(
            icon: Icon( Icons.delete_forever ), 
            onPressed: (){

              Provider.of<ScanListProvider>(context, listen: false)
                .borrarTodos();

            }
          )
        ],
      ),
      drawer: MenuWidget(),
      body: _HomePageBody(),

     bottomNavigationBar: CustomNavigationBar(),
     floatingActionButton: ScanButton(),
     floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
   );
  }
}


class _HomePageBody extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {

    // Obtener el selected menu opt
    final uiProvider = Provider.of<UiProvider>(context);
    
    // Cambiar para mostrar la pagina respectiva
    final currentIndex = uiProvider.selectedMenuOpt;

    // Usar el ScanListProvider
    final scanListProvider = Provider.of<ScanListProvider>(context, listen: false);

    switch( currentIndex ) {

      case 0:
        scanListProvider.cargarScanPorTipo('geo');
        return MapasPage();

      case 1: 
        scanListProvider.cargarScanPorTipo('http');
        return DireccionesPage();

      default:
        return MapasPage();
    }


  }
}