import 'dart:ui';

import 'package:VacunatePeru/src/models/paciente_model.dart';

import 'package:VacunatePeru/src/providers/paciente_provider.dart';
import 'package:VacunatePeru/src/theme/theme.dart';
import 'package:VacunatePeru/src/widgets/menu_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:animate_do/animate_do.dart';
import 'package:VacunatePeru/src/pages/paciente_id_page.dart';
class PacientePage extends StatefulWidget {
  PacientePage({Key key}) : super(key: key);

  @override
  _PacientePageState createState() => _PacientePageState();
}

class _PacientePageState extends State<PacientePage> {
  final pacienteProvider = new PacienteProvider();
  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Paciente'),
      ),
      drawer: MenuWidget(),
      body: _viewPaciente(),
    );
  }

  Widget _viewPaciente() {
    return FutureBuilder(
      future: pacienteProvider.cargarPaciente(),
      builder:
          (BuildContext context, AsyncSnapshot<List<PacienteModel>> snapshot) {
        if (snapshot.hasData) {
          final pacientelista = snapshot.data;
          print(pacientelista);
          return ListView.builder(
            itemCount: pacientelista.length,
            itemBuilder: (context, i) => _crearItem(context, pacientelista[i]),
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    );
  }

  Widget _crearItem(BuildContext context, PacienteModel paciente) {
    // print(paciente);
    return Card(
      elevation: 5.0,
      
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        children: <Widget>[
             ( paciente.foto == null ) 
              ? Image(image: AssetImage('assets/no-image.png'))
              : FadeInLeft(
                          child: ListTile(
                leading: ClipRRect(
                        borderRadius: new BorderRadius.circular(5.0),
                  
                                  child: FadeInImage(
                                  
                    placeholder: AssetImage('assets/FEDTpyE.gif'),
                    image: NetworkImage( paciente.foto),
                    
                    fit: BoxFit.cover,
                  ),
                ),  
                
                title: Text(' ${ paciente.nombre} ${paciente.apellido}',style:TextStyle(fontSize: 15),),
                subtitle: Text('DNI: ${ paciente.dni}',style: TextStyle( color: Colors.grey)),
                trailing: Text('Dosis: ${paciente.dosis}'),
                //onTap: () => Navigator.pushNamed(context, 'producto', arguments: producto ), // lo cambie para actualizar home
                 onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PacienteIDPage(todo:paciente),
                          ),
                        );


                        
                      },
              ),
            ),

        ],
      ),
    );
  }
}
