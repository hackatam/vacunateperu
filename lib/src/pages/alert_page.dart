  
import 'package:flutter/material.dart';


class AlertPage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Center(child: Text('Validación de Fases')),
      ),
      body:  _mostrarAlert(context),
    );
      
    
    
  }


  Widget _mostrarAlert(BuildContext context) {


        return Container(
          color: Colors.grey,
                  child: AlertDialog(
            shape: RoundedRectangleBorder( borderRadius: BorderRadius.circular(20.0) ),
            title: Text('Titulo'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('Ustes si pertenece a la "Primera Fase" de Vacunación',style: TextStyle(color: Color(0xff00578b),fontSize: 24.0,
                  fontWeight: FontWeight.bold),textAlign: TextAlign.center,),
                 Center(
                      child: Container(
                        width: 280.0,
                        height: 280.0,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(60.0),
                            image: DecorationImage(
                              image: AssetImage('assets/logomenu2.png'),
                            )),
                      ),
                    ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Cancelar',style: TextStyle(fontSize: 18.0,
                  fontWeight: FontWeight.bold),),
                onPressed: ()=> Navigator.pushReplacementNamed(context, 'register'),
              ),
              FlatButton(
                child: Text('Siguiente',style: TextStyle(fontSize: 18.0,
                  fontWeight: FontWeight.bold),),
                onPressed: (){
                  Navigator.pushReplacementNamed(context, 'mapa');
                },
              ),
            ],
          ),
        );


  }

}