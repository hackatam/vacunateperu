import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:VacunatePeru/src/theme/theme.dart';
import 'package:provider/provider.dart';

class MenuWidget extends StatelessWidget {

  final colorbase    = Color(0xff01a745);
  final _estiloTexto =new TextStyle(fontSize:15, color: Color(0xff01a745));

  @override
  Widget build(BuildContext context) {
      final _screenSize = MediaQuery.of(context).size; 

    final appTheme = Provider.of<ThemeChanger>(context);
    final accentColor = appTheme.currentTheme.accentColor;
    final appTheme2 = Provider.of<ThemeChanger>(context).currentTheme;

    return SafeArea(
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            
            DrawerHeader(
              
              
              child: Container(),
              
              decoration: BoxDecoration(
                
                
                
                  image: DecorationImage(
                
                image: AssetImage('assets/logomenu2.png'),
                //fit: BoxFit.cover
              )
              ),
            ),

              /*Container(

                child: ListTile(
                  leading: Icon(Icons.home_work, color: appTheme2.accentColor),
                  title: Text('Home',style: TextStyle(color:accentColor)),
                  trailing: Icon(Icons.keyboard_arrow_right,color: appTheme2.accentColor),
                  onTap: () => Navigator.pushReplacementNamed(context, 'home'),
                ),
                
                            ),
                            Divider(),*/
              Container(
                child: ListTile(
                  leading: Icon(Icons.map , color: appTheme2.accentColor),
                  title: Text('Mapa',style: TextStyle(color:accentColor)),
                  trailing: Icon(Icons.keyboard_arrow_right,color: appTheme2.accentColor),
                  onTap: () => Navigator.pushReplacementNamed(context, 'mapa'),
                ),
                
                            ),
                            Divider(),
                                          Container(
                child: ListTile(
                  leading: Icon(Icons.person , color: appTheme2.accentColor),
                  title: Text('Perfil',style: TextStyle(color:accentColor)),
                  trailing: Icon(Icons.keyboard_arrow_right,color: appTheme2.accentColor),
                  onTap: () => Navigator.pushReplacementNamed(context, 'Nasca'),
                ),
                
                            ),
                            Divider(),
              Container(
               child: ListTile(
                leading: Icon(Icons.pages,color: appTheme2.accentColor),
                title: Text('Scan',style: TextStyle(color:accentColor)),
                trailing: Icon(Icons.keyboard_arrow_right,color: appTheme2.accentColor),
                onTap: () => Navigator.pushReplacementNamed(context, 'scan'),
              ),
                          ),
                          Divider(),

            /*Container(
              child: ListTile(
                leading: Icon(Icons.group, color: appTheme2.accentColor),
                title: Text('Equipo',style: TextStyle(color:accentColor)),
                trailing: Icon(Icons.keyboard_arrow_right,color: appTheme2.accentColor),
                onTap: () => Navigator.pushReplacementNamed(context, 'equipo'),
              ),
                          ),
                          Divider(),*/

              Container(
                child: ListTile(
                
                leading: Icon(Icons.medical_services, color: appTheme2.accentColor),
                title: Text('Pacientes',style: TextStyle(color:accentColor)),
                 trailing: Icon(Icons.keyboard_arrow_right,color: appTheme2.accentColor),
                onTap: () => Navigator.pushReplacementNamed(context, 'Paciente'),
              ),
                          ),
                          Divider(),
            /*Container(

              child: ListTile(
                
                leading: Icon(Icons.video_collection_outlined, color: appTheme2.accentColor),
                title: Text('Videos JA',style: TextStyle(color:accentColor)),
                trailing: Icon(Icons.keyboard_arrow_right,color: appTheme2.accentColor),
                onTap: () => Navigator.pushReplacementNamed(context, 'video'),
              ),
                          ),
                          Divider(),*/

              Container(

                child: ListTile(
                              
                  leading: Icon(Icons.double_arrow_outlined, color: appTheme2.accentColor),
                  title: Text('Salir',style: TextStyle(color:accentColor)),
                  trailing: Icon(Icons.keyboard_arrow_right,color: appTheme2.accentColor),
                  onTap: () {
                    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                  }),
                          ),
           Divider(),
            SizedBox(height: 20),

              ListTile(
              leading: Icon( Icons.lightbulb_outline, color: accentColor ),
              title: Text('Dark Mode',style: TextStyle(color:accentColor)),
              trailing: Switch.adaptive(
                value: appTheme.darkTheme ,
                activeColor: accentColor,
                onChanged: ( value ) => appTheme.darkTheme = value
              ),
            ),


            SafeArea(
              bottom: true,
              top: false,
              left: false,
              right: false,
              child: ListTile(
                leading: Icon( Icons.add_to_home_screen, color: accentColor ),
                title: Text('Custom Theme',style: TextStyle(color:accentColor)),
                trailing: Switch.adaptive(
                  
                  value: appTheme.customTheme, 
                  activeColor: accentColor,
                  onChanged: ( value ) => appTheme.customTheme = value
                ),
              ),
            ),

          ],
        ),
      ),
      
    );

  }
 
}
