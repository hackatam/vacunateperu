

import 'package:VacunatePeru/src/pages/alert_page.dart';
import 'package:VacunatePeru/src/pages/calendar.dart';

import 'package:VacunatePeru/src/pages/registro_page.dart';
import 'package:VacunatePeru/src/pages/registro_principal.dart';
import 'package:VacunatePeru/src/pages/scan.dart';
import 'package:flutter/material.dart';
import 'package:VacunatePeru/src/pages/equipo_page.dart';

//import 'package:formvalidation/src/bloc/provider.dart';

import 'package:VacunatePeru/src/pages/home_page.dart';

import 'package:VacunatePeru/src/pages/jlab_id_podcast_page.dart';
import 'package:VacunatePeru/src/pages/mapa.dart';
import 'package:VacunatePeru/src/pages/splash_screen.dart';
import 'package:VacunatePeru/src/pages/videos_page.dart';
import 'package:VacunatePeru/src/pages/jlab_podcast_page.dart';
import 'package:VacunatePeru/src/pages/programa_id_page.dart';
import 'package:VacunatePeru/src/pages/programas_page.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:VacunatePeru/src/theme/theme.dart';
import 'package:provider/provider.dart';
import 'dart:async';
import 'package:camera/camera.dart';
import 'package:VacunatePeru/src/pages/nasca.dart'; 
import 'package:VacunatePeru/src/providers/scan_list_provider.dart';
import 'package:VacunatePeru/src/providers/ui_provider.dart';
import 'package:VacunatePeru/src/pages/Paciente_id_page.dart';
import 'package:VacunatePeru/src/pages/perfil_paciente_page.dart';
 List<CameraDescription> cameras;
Future<Null> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  try {
    cameras = await availableCameras();
  } on CameraException catch (e) {
    print('Error: $e.code\nError Message: $e.message');
  }
  runApp(

    ChangeNotifierProvider(
      create: (_) => new ThemeChanger( 1 ),
      child: MyApp()
    )
  );
}
 
class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    final currentTheme = Provider.of<ThemeChanger>(context).currentTheme;
    //return Provider(
      //child: MaterialApp(
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => new UiProvider() ),
        ChangeNotifierProvider(create: (_) => new ScanListProvider() ),
      ],
              child: MaterialApp(
          theme: currentTheme,
          debugShowCheckedModeBanner: false,
                localizationsDelegates: [
         GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', 'US'), // English
          const Locale('es', 'ES'),
        ],
          title: 'JA PERÚ',
          initialRoute: 'splash',
          routes: {
            'splash'     : ( BuildContext context ) => SplashScreenPage(),
            'home'     : ( BuildContext context ) => HomePage(),
            'mapa'     : ( BuildContext context ) => MapaPage(),
            'programas'     : ( BuildContext context ) => ProgramasPage(),
            'programa'     : ( BuildContext context ) => ProgramaIdPage(),
            'equipo'     : ( BuildContext context ) => EquipoPage(),
            'video'     : ( BuildContext context ) => VideosPage(),
            'jlabPodcast'     : ( BuildContext context ) => JLabPodcastPage(),
            'jlabPodcastID'     : ( BuildContext context ) => PodcastIdPage(),
            'registro'     : ( BuildContext context ) => InputPage(),
            'register'     : ( BuildContext context ) => LoginePage(),
            'alerta'     : ( BuildContext context ) => AlertPage(),
            'calendar'     : ( BuildContext context ) => CalendarPage(),
            'scan'     : ( BuildContext context ) => ScanPage(),
            'Nasca': (BuildContext context) => NascaPage(),
            //'Pacientes': (BuildContext context) => PacienteIDPage(),
            'Paciente': (BuildContext context) => PacientePage(),
          },
          /*theme: ThemeData(
            primaryColor: Color(0xff01a745),
            floatingActionButtonTheme: FloatingActionButtonThemeData(
              backgroundColor: Color(0xff01a745)
          )
          ),*/
        ),
      );
    //);
      
  }
}